import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Badge from '@mui/material/Badge';
import { useSelector } from 'react-redux';





const Header = () => {
    const todos = useSelector(state => state.todos.data.length)
    return (

        < Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>

                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Add to do!
                    </Typography>
                    <Badge badgeContent={todos} color="warning">
                        <MenuIcon />
                    </Badge>
                </Toolbar>
            </AppBar>
        </Box >
    )
}

export default Header; 