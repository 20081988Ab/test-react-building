import Accordion from '@mui/material/Accordion';
import AccordionActions from '@mui/material/AccordionActions';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Grid from '@mui/material/Grid';
import Checkbox from '@mui/material/Checkbox';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const TodoItem = ({ title = "", description = "", isDone = false }) => {


    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    return (
        <Box sx={{ padding: 2 }}>
            <Grid container spacing={2} xs={{ m: 2 }}>
                <Grid item xs={10}>
                    <Accordion>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1-content"
                            id="panel1-header"
                        >
                            {title}
                        </AccordionSummary>
                        <AccordionDetails>
                            {description}
                        </AccordionDetails>
                    </Accordion>

                </Grid>

                <Grid item xs={2}>
                    <Checkbox {...label} checked={isDone} />
                </Grid>

            </Grid>
        </Box>
    )
}

TodoItem.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    isDone: PropTypes.bool
};

export default TodoItem;