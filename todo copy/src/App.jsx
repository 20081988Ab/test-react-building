import Header from "./components/Header"
import Typography from '@mui/material/Typography';
import smileIcon from "../src/emoticon-happy.svg"
import Tooltip from '@mui/material/Tooltip';
import styles from "./App.module.scss"
import FormAdd from "./components/FormAddd";
import TodoItem from "./components/TodoItem";
import { useDispatch, useSelector } from "react-redux"
import { fetchAllTodo } from "./redux/todoSlice";
import { useEffect } from "react";


function App() {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchAllTodo())
  },[])
 
  const todos = useSelector(state => state.todos.data)

  return (
    <>
      <Header />
      <div>
        <Typography className={styles.title} sx={{ m: 3 }} variant="h3">
          Add your todo now!

          <div className={styles.iconWrapper}>
            <Tooltip title="Fill the form and press the button" placement="top-start">
              <img src={smileIcon} width={40} height={40} alt="emoji" />
            </Tooltip>
          </div>
        </Typography>
      </div>
      <FormAdd />
      <br />
      <hr />
      <br />

      {todos.map(todo => <TodoItem key={todo.id} {...todo} />)}
    </>
  )
}

export default App
